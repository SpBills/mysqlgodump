# mysqlgodump
A simple library that allows you to export mysql files from a given host to a given directory.

DISCLAIMER: mysqldump must be installed on the machine utilizing this package.

Example code:
```go
package main

import (
    "github.com/spbills/mysqlgodump"
    "fmt"
)

func main() {
    username := "root"
    password := "<INSERT YOUR PASSWORD>"
    database := "<INSERT DATABASE NAME>"

    // port will go inside of the hostname.
    hostname := "localhost:3306"

    // Make sure you put the final / in.
    directory := "~/data/"

    // register the dump.
    dumper := mysqlgodump.RegisterDump(username, password, database, hostname)

    // perform the query, sending in the directory you wish to export to.
    resultFileName, err := dumper.Export(directory)
    
    fmt.Println(resultFileName)
    // ${directory}${database}-${unixtimestamp}.sql
}
```

## TODO: 
* Add connection testing to the register method so it actually has a use, aside from being a glorified constructor.
