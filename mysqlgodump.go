package mysqlgodump

import (
	"fmt"
	"os/exec"
	"time"
)

// DumpData contains all data for the database to do its job.
type DumpData struct {
	username string
	password string
	database string
	hostname string
}

// RegisterDump registers the dumper. basically a constructor.
// Parameters:
// - username
// - password
// - database name
// - hostname (ip/dns)
// Returns:
// - d > DumpData object for later use.
func RegisterDump(user, pass, db, hostname string) *DumpData {
	return &DumpData{
		username: user,
		password: pass,
		database: db,
		hostname: hostname,
	}
}

// Export uses data provided by the "constructor" to export to a directory.
// All files exported are in the form:
// dbname-unixtimecode.sql
// Parameters:
// - dir > The directory which the program is meant to export the file to (Blank for current directory).
// Returns:
// - finalName > The final name of the file. This is needed because of importing later in the program, assumedly.
// - error > Error from the OS out.
func (d *DumpData) Export(dir string) (finalName string, err error) {
	finalName = fmt.Sprintf("%s/%s-%s.sql", dir, d.database, time.Now().Format("20060102150405"))
	cmd := exec.Command("mysqldump", fmt.Sprintf("-u%s", d.username), fmt.Sprintf("-p%s", d.password), d.database, "-r", finalName)

	err = cmd.Run()

	return finalName, err
}
